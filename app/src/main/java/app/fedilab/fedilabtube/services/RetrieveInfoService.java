package app.fedilab.fedilabtube.services;
/* Copyright 2020 Thomas Schneider
 *
 * This file is a part of TubeLab
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * TubeLab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with TubeLab; if not,
 * see <http://www.gnu.org/licenses>. */

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.IBinder;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import java.util.LinkedHashMap;
import java.util.Objects;

import app.fedilab.fedilabtube.R;
import app.fedilab.fedilabtube.client.RetrofitPeertubeAPI;
import app.fedilab.fedilabtube.client.entities.PeertubeInformation;
import app.fedilab.fedilabtube.helper.EmojiHelper;
import app.fedilab.fedilabtube.helper.NetworkStateReceiver;

import static app.fedilab.fedilabtube.helper.Helper.peertubeInformation;


public class RetrieveInfoService extends Service implements NetworkStateReceiver.NetworkStateReceiverListener {

    static String NOTIFICATION_CHANNEL_ID = "update_info_peertube";
    private NetworkStateReceiver networkStateReceiver;


    public void onCreate() {
        super.onCreate();
        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));


    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_ID,
                    getString(R.string.notification_channel_name),
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setSound(null, null);

            ((NotificationManager) Objects.requireNonNull(getSystemService(Context.NOTIFICATION_SERVICE))).createNotificationChannel(channel);
            android.app.Notification notification = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                    .setSmallIcon(R.drawable.ic_notification_tubelab)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText(getString(R.string.notification_channel_name))
                    .setAutoCancel(true).build();

            startForeground(1, notification);

        } else {
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                    .setContentTitle(getString(R.string.app_name))
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setContentText(getString(R.string.notification_channel_name))
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setAutoCancel(true);

            Notification notification = builder.build();

            startForeground(1, notification);
        }
        Thread thread = new Thread() {

            @Override
            public void run() {
                EmojiHelper.fillMapEmoji(getApplicationContext());
                peertubeInformation = new PeertubeInformation();
                peertubeInformation.setCategories(new LinkedHashMap<>());
                peertubeInformation.setLanguages(new LinkedHashMap<>());
                peertubeInformation.setLicences(new LinkedHashMap<>());
                peertubeInformation.setPrivacies(new LinkedHashMap<>());
                peertubeInformation.setPlaylistPrivacies(new LinkedHashMap<>());
                peertubeInformation.setTranslations(new LinkedHashMap<>());
                peertubeInformation = new RetrofitPeertubeAPI(RetrieveInfoService.this).getPeertubeInformation();
                stopForeground(true);
            }
        };
        thread.start();
        return START_NOT_STICKY;
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (networkStateReceiver != null) {
            networkStateReceiver.removeListener(this);
            unregisterReceiver(networkStateReceiver);
        }
    }

    @Override
    public void networkAvailable() {
        Thread thread = new Thread() {

            @Override
            public void run() {
                EmojiHelper.fillMapEmoji(getApplicationContext());
                if (peertubeInformation == null || peertubeInformation.getCategories().size() == 0) {
                    peertubeInformation = new PeertubeInformation();
                    peertubeInformation.setCategories(new LinkedHashMap<>());
                    peertubeInformation.setLanguages(new LinkedHashMap<>());
                    peertubeInformation.setLicences(new LinkedHashMap<>());
                    peertubeInformation.setPrivacies(new LinkedHashMap<>());
                    peertubeInformation.setPlaylistPrivacies(new LinkedHashMap<>());
                    peertubeInformation.setTranslations(new LinkedHashMap<>());
                    peertubeInformation = new RetrofitPeertubeAPI(RetrieveInfoService.this).getPeertubeInformation();
                }
                stopForeground(true);
            }
        };
        thread.start();
    }

    @Override
    public void networkUnavailable() {

    }
}
