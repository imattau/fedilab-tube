package app.fedilab.fedilabtube;
/* Copyright 2020 Thomas Schneider
 *
 * This file is a part of TubeLab
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * TubeLab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with TubeLab; if not,
 * see <http://www.gnu.org/licenses>. */

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import app.fedilab.fedilabtube.client.APIResponse;
import app.fedilab.fedilabtube.client.RetrofitPeertubeAPI;
import app.fedilab.fedilabtube.client.data.AccountData;
import app.fedilab.fedilabtube.client.data.ChannelData.Channel;
import app.fedilab.fedilabtube.drawer.OwnAccountsAdapter;
import app.fedilab.fedilabtube.fragment.DisplayAccountsFragment;
import app.fedilab.fedilabtube.fragment.DisplayVideosFragment;
import app.fedilab.fedilabtube.helper.Helper;
import app.fedilab.fedilabtube.sqlite.AccountDAO;
import app.fedilab.fedilabtube.sqlite.Sqlite;
import app.fedilab.fedilabtube.viewmodel.ChannelsVM;
import app.fedilab.fedilabtube.viewmodel.PostActionsVM;
import app.fedilab.fedilabtube.viewmodel.RelationshipVM;
import app.fedilab.fedilabtube.viewmodel.TimelineVM;
import es.dmoral.toasty.Toasty;

import static androidx.core.text.HtmlCompat.FROM_HTML_MODE_LEGACY;
import static app.fedilab.fedilabtube.MainActivity.TypeOfConnection.SURFING;
import static app.fedilab.fedilabtube.client.RetrofitPeertubeAPI.ActionType.FOLLOW;
import static app.fedilab.fedilabtube.client.RetrofitPeertubeAPI.ActionType.MUTE;
import static app.fedilab.fedilabtube.client.RetrofitPeertubeAPI.ActionType.REPORT_ACCOUNT;
import static app.fedilab.fedilabtube.client.RetrofitPeertubeAPI.ActionType.UNFOLLOW;
import static app.fedilab.fedilabtube.client.RetrofitPeertubeAPI.DataType.CHANNEL;
import static app.fedilab.fedilabtube.helper.Helper.isLoggedIn;


public class ShowChannelActivity extends AppCompatActivity {


    private Button account_follow;
    private ViewPager mPager;
    private TabLayout tabLayout;
    private TextView account_note, subscriber_count;
    private Map<String, Boolean> relationship;
    private ImageView account_pp;
    private TextView account_dn;
    private Channel channel;
    private action doAction;
    private String channelAcct;
    private boolean sepiaSearch;
    private String peertubeInstance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_channel);
        setTitle("");
        Bundle b = getIntent().getExtras();
        account_follow = findViewById(R.id.account_follow);
        subscriber_count = findViewById(R.id.subscriber_count);
        account_follow.setEnabled(false);
        account_pp = findViewById(R.id.account_pp);
        account_dn = findViewById(R.id.account_dn);
        account_pp.setBackgroundResource(R.drawable.account_pp_border);
        if (b != null) {
            channel = b.getParcelable("channel");
            channelAcct = b.getString("channelId");
            sepiaSearch = b.getBoolean("sepia_search", false);
            peertubeInstance = b.getString("peertube_instance", null);

        } else {
            Toasty.error(ShowChannelActivity.this, getString(R.string.toast_error_loading_account), Toast.LENGTH_LONG).show();
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        tabLayout = findViewById(R.id.account_tabLayout);
        account_note = findViewById(R.id.account_note);


        ChannelsVM viewModel = new ViewModelProvider(ShowChannelActivity.this).get(ChannelsVM.class);
        viewModel.get(sepiaSearch ? peertubeInstance : null, CHANNEL, channelAcct == null ? channel.getAcct() : channelAcct).observe(ShowChannelActivity.this, this::manageViewAccounts);
        manageChannel();

        if (MainActivity.typeOfConnection == MainActivity.TypeOfConnection.SURFING) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                int[][] states = new int[][]{
                        new int[]{android.R.attr.state_enabled}, // enabled
                        new int[]{-android.R.attr.state_enabled}, // disabled
                        new int[]{-android.R.attr.state_checked}, // unchecked
                        new int[]{android.R.attr.state_pressed}  // pressed
                };

                int[] colors = new int[]{
                        ContextCompat.getColor(ShowChannelActivity.this, Helper.getColorAccent()),
                        ContextCompat.getColor(ShowChannelActivity.this, Helper.getColorAccent()),
                        ContextCompat.getColor(ShowChannelActivity.this, Helper.getColorAccent()),
                        ContextCompat.getColor(ShowChannelActivity.this, Helper.getColorAccent())
                };
                account_follow.setBackgroundTintList(new ColorStateList(states, colors));
            }
            account_follow.setText(getString(R.string.action_follow));
            account_follow.setEnabled(true);
            new Thread(() -> {
                try {
                    SQLiteDatabase db = Sqlite.getInstance(getApplicationContext(), Sqlite.DB_NAME, null, Sqlite.DB_VERSION).open();
                    List<AccountData.Account> accounts = new AccountDAO(ShowChannelActivity.this, db).getAllAccount();
                    runOnUiThread(() -> {
                        account_follow.setVisibility(View.VISIBLE);
                        account_follow.setOnClickListener(v -> {
                            AlertDialog.Builder builderSingle = new AlertDialog.Builder(ShowChannelActivity.this);
                            builderSingle.setTitle(getString(R.string.list_of_accounts));
                            if (accounts != null && accounts.size() > 0) {
                                if (accounts.size() > 1) {
                                    final OwnAccountsAdapter accountsListAdapter = new OwnAccountsAdapter(ShowChannelActivity.this, accounts);
                                    builderSingle.setAdapter(accountsListAdapter, (dialog, which) -> new Thread(() -> {
                                        try {
                                            RetrofitPeertubeAPI peertubeAPI = new RetrofitPeertubeAPI(ShowChannelActivity.this, accounts.get(which).getHost(), accounts.get(which).getToken());
                                            peertubeAPI.post(FOLLOW, channel.getAcct(), null);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }).start());
                                } else {
                                    RetrofitPeertubeAPI peertubeAPI = new RetrofitPeertubeAPI(ShowChannelActivity.this, accounts.get(0).getHost(), accounts.get(0).getToken());
                                    peertubeAPI.post(FOLLOW, channel.getAcct(), null);
                                }
                            }
                            builderSingle.setNegativeButton(R.string.cancel, (dialog, which) -> dialog.dismiss());
                            builderSingle.show();
                        });
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }).start();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(@NotNull Menu menu) {
        getMenuInflater().inflate(R.menu.main_account, menu);
        if (!Helper.isLoggedIn(ShowChannelActivity.this) || sepiaSearch) {
            menu.findItem(R.id.action_mute).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else if (item.getItemId() == R.id.action_mute) {
            PostActionsVM viewModel = new ViewModelProvider(ShowChannelActivity.this).get(PostActionsVM.class);
            viewModel.post(MUTE, channel.getOwnerAccount().getAcct(), null).observe(ShowChannelActivity.this, apiResponse -> manageVIewPostActions(MUTE, apiResponse));
        } else if (item.getItemId() == R.id.action_report) {
            androidx.appcompat.app.AlertDialog.Builder dialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(ShowChannelActivity.this);
            LayoutInflater inflater1 = getLayoutInflater();
            View dialogView = inflater1.inflate(R.layout.popup_report, new LinearLayout(ShowChannelActivity.this), false);
            dialogBuilder.setView(dialogView);
            EditText report_content = dialogView.findViewById(R.id.report_content);
            dialogBuilder.setNeutralButton(R.string.cancel, (dialog, id) -> dialog.dismiss());
            dialogBuilder.setPositiveButton(R.string.report, (dialog, id) -> {
                if (report_content.getText().toString().trim().length() == 0) {
                    Toasty.info(ShowChannelActivity.this, getString(R.string.report_comment_size), Toasty.LENGTH_LONG).show();
                } else {
                    PostActionsVM viewModel = new ViewModelProvider(ShowChannelActivity.this).get(PostActionsVM.class);
                    viewModel.post(REPORT_ACCOUNT, channel.getId(), report_content.getText().toString()).observe(ShowChannelActivity.this, apiResponse -> manageVIewPostActions(REPORT_ACCOUNT, apiResponse));
                    dialog.dismiss();
                }
            });
            androidx.appcompat.app.AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.show();
        } else if (item.getItemId() == R.id.action_share && channel != null) {
            Intent sendIntent = new Intent(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.shared_via));
            String extra_text = channel.getUrl();
            sendIntent.putExtra(Intent.EXTRA_TEXT, extra_text);
            sendIntent.setType("text/plain");
            try {
                startActivity(Intent.createChooser(sendIntent, getString(R.string.share_with)));
            } catch (Exception e) {
                Toasty.error(ShowChannelActivity.this, getString(R.string.toast_error), Toasty.LENGTH_LONG).show();
            }
        } else if (item.getItemId() == R.id.action_display_account) {
            Bundle b = new Bundle();
            Intent intent = new Intent(ShowChannelActivity.this, ShowAccountActivity.class);
            b.putParcelable("account", channel.getOwnerAccount());
            b.putString("accountAcct", channel.getOwnerAccount().getAcct());
            intent.putExtras(b);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    private void manageChannel() {
        SharedPreferences sharedpreferences = getSharedPreferences(Helper.APP_PREFS, MODE_PRIVATE);

        String accountIdRelation = channel.getAcct();
        if (isLoggedIn(ShowChannelActivity.this)) {
            RelationshipVM viewModel = new ViewModelProvider(ShowChannelActivity.this).get(RelationshipVM.class);
            List<String> uids = new ArrayList<>();
            uids.add(accountIdRelation);
            viewModel.get(uids).observe(ShowChannelActivity.this, this::manageVIewRelationship);
        }

        setTitle(channel.getAcct());


        mPager = findViewById(R.id.account_viewpager);
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.videos)));
        mPager.setOffscreenPageLimit(1);


        PagerAdapter mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        ViewGroup.LayoutParams params = tabLayout.getLayoutParams();
        params.height = 0;
        tabLayout.setLayoutParams(params);
        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                TabLayout.Tab tab = tabLayout.getTabAt(position);
                if (tab != null)
                    tab.select();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                Fragment fragment = null;
                if (mPager.getAdapter() != null)
                    fragment = (Fragment) mPager.getAdapter().instantiateItem(mPager, tab.getPosition());
                switch (tab.getPosition()) {
                    case 0:
                        if (fragment != null) {
                            DisplayVideosFragment displayVideosFragment = ((DisplayVideosFragment) fragment);
                            displayVideosFragment.scrollToTop();
                        }
                        break;
                    case 1:
                        if (fragment != null) {
                            DisplayAccountsFragment displayAccountsFragment = ((DisplayAccountsFragment) fragment);
                            displayAccountsFragment.scrollToTop();
                        }
                        break;
                }
            }
        });

        account_dn.setText(channel.getDisplayName());


        manageNotes(channel);
        Helper.loadGiF(ShowChannelActivity.this, sepiaSearch ? peertubeInstance : null, channel.getAvatar() != null ? channel.getAvatar().getPath() : null, account_pp);
        //Follow button
        String target = channel.getAcct();

        account_follow.setOnClickListener(v -> {
            if (doAction == action.NOTHING) {
                Toasty.info(ShowChannelActivity.this, getString(R.string.nothing_to_do), Toast.LENGTH_LONG).show();
            } else if (doAction == action.FOLLOW) {
                account_follow.setEnabled(false);
                PostActionsVM viewModel = new ViewModelProvider(ShowChannelActivity.this).get(PostActionsVM.class);
                viewModel.post(FOLLOW, target, null).observe(ShowChannelActivity.this, apiResponse -> manageVIewPostActions(FOLLOW, apiResponse));
            } else if (doAction == action.UNFOLLOW) {
                boolean confirm_unfollow = sharedpreferences.getBoolean(Helper.SET_UNFOLLOW_VALIDATION, true);
                if (confirm_unfollow) {
                    AlertDialog.Builder unfollowConfirm = new AlertDialog.Builder(ShowChannelActivity.this);
                    unfollowConfirm.setTitle(getString(R.string.unfollow_confirm));
                    unfollowConfirm.setMessage(channel.getAcct());
                    unfollowConfirm.setNegativeButton(R.string.cancel, (dialog, which) -> dialog.dismiss());
                    unfollowConfirm.setPositiveButton(R.string.yes, (dialog, which) -> {
                        account_follow.setEnabled(false);
                        PostActionsVM viewModel = new ViewModelProvider(ShowChannelActivity.this).get(PostActionsVM.class);
                        viewModel.post(UNFOLLOW, target, null).observe(ShowChannelActivity.this, apiResponse -> manageVIewPostActions(UNFOLLOW, apiResponse));
                        dialog.dismiss();
                    });
                    unfollowConfirm.show();
                } else {
                    account_follow.setEnabled(false);
                    PostActionsVM viewModel = new ViewModelProvider(ShowChannelActivity.this).get(PostActionsVM.class);
                    viewModel.post(UNFOLLOW, target, null).observe(ShowChannelActivity.this, apiResponse -> manageVIewPostActions(UNFOLLOW, apiResponse));
                }

            }
        });
    }


    public void manageVIewRelationship(APIResponse apiResponse) {

        if (apiResponse.getError() != null) {
            if (apiResponse.getError().getError().length() > 500) {
                Toasty.info(ShowChannelActivity.this, getString(R.string.remote_account), Toast.LENGTH_LONG).show();
            } else {
                Toasty.error(ShowChannelActivity.this, apiResponse.getError().getError(), Toast.LENGTH_LONG).show();
            }
            return;
        }
        this.relationship = apiResponse.getRelationships();
        manageButtonVisibility();

        invalidateOptionsMenu();

    }

    //Manages the visibility of the button
    private void manageButtonVisibility() {
        if (relationship == null || MainActivity.typeOfConnection == SURFING)
            return;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int[][] states = new int[][]{
                    new int[]{android.R.attr.state_enabled}, // enabled
                    new int[]{-android.R.attr.state_enabled}, // disabled
                    new int[]{-android.R.attr.state_checked}, // unchecked
                    new int[]{android.R.attr.state_pressed}  // pressed
            };

            int[] colors = new int[]{
                    ContextCompat.getColor(ShowChannelActivity.this, Helper.getColorAccent()),
                    ContextCompat.getColor(ShowChannelActivity.this, Helper.getColorAccent()),
                    ContextCompat.getColor(ShowChannelActivity.this, Helper.getColorAccent()),
                    ContextCompat.getColor(ShowChannelActivity.this, Helper.getColorAccent())
            };
            account_follow.setBackgroundTintList(new ColorStateList(states, colors));
        }
        account_follow.setEnabled(true);

        boolean isFollowing = relationship.get(channel.getAcct());
        if (isFollowing) {
            account_follow.setText(R.string.action_unfollow);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                account_follow.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(ShowChannelActivity.this, R.color.red_1)));
            }
            doAction = action.UNFOLLOW;
        } else {
            account_follow.setText(R.string.action_follow);
            doAction = action.FOLLOW;
        }
        account_follow.setVisibility(View.VISIBLE);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onStop() {
        super.onStop();
    }


    public void manageVIewPostActions(RetrofitPeertubeAPI.ActionType statusAction, APIResponse apiResponse) {

        if (apiResponse.getError() != null) {
            Toasty.error(ShowChannelActivity.this, apiResponse.getError().getError(), Toast.LENGTH_LONG).show();
            return;
        }
        String target = channel.getAcct();
        //IF action is unfollow or mute, sends an intent to remove statuses
        if (statusAction == RetrofitPeertubeAPI.ActionType.UNFOLLOW) {
            Bundle b = new Bundle();
            b.putString("receive_action", apiResponse.getTargetedId());
            Intent intentBC = new Intent(Helper.RECEIVE_ACTION);
            intentBC.putExtras(b);
        }
        if (statusAction == RetrofitPeertubeAPI.ActionType.UNFOLLOW || statusAction == RetrofitPeertubeAPI.ActionType.FOLLOW) {
            RelationshipVM viewModel = new ViewModelProvider(ShowChannelActivity.this).get(RelationshipVM.class);
            List<String> uris = new ArrayList<>();
            uris.add(target);
            viewModel.get(uris).observe(ShowChannelActivity.this, this::manageVIewRelationship);
        } else if (statusAction == RetrofitPeertubeAPI.ActionType.MUTE) {
            Toasty.info(ShowChannelActivity.this, getString(R.string.muted_done), Toast.LENGTH_LONG).show();
        }
    }

    public void manageViewAccounts(APIResponse apiResponse) {
        if (apiResponse.getChannels() != null && apiResponse.getChannels().size() == 1) {
            Channel channel = apiResponse.getChannels().get(0);
            if (this.channel == null) {
                this.channel = channel;
                manageChannel();
            }
            if (channel.getOwnerAccount() != null) {
                this.channel.setOwnerAccount(channel.getOwnerAccount());
            }
            subscriber_count.setText(getString(R.string.followers_count, Helper.withSuffix(channel.getFollowersCount())));
            subscriber_count.setVisibility(View.VISIBLE);
            manageNotes(channel);
        }
    }

    private void manageNotes(Channel channel) {
        if (channel.getDescription() != null && channel.getDescription().compareTo("null") != 0 && channel.getDescription().trim().length() > 0) {
            SpannableString spannableString;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                spannableString = new SpannableString(Html.fromHtml(channel.getDescription(), FROM_HTML_MODE_LEGACY));
            else
                spannableString = new SpannableString(Html.fromHtml(channel.getDescription()));

            account_note.setText(spannableString, TextView.BufferType.SPANNABLE);
            account_note.setMovementMethod(LinkMovementMethod.getInstance());
            account_note.setVisibility(View.VISIBLE);
        } else {
            account_note.setVisibility(View.GONE);
        }
    }

    public enum action {
        FOLLOW,
        UNFOLLOW,
        NOTHING
    }

    /**
     * Pager adapter for the 2 fragments
     */
    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

        ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        }

        @NotNull
        @Override
        public Fragment getItem(int position) {
            DisplayVideosFragment displayVideosFragment = new DisplayVideosFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable(Helper.TIMELINE_TYPE, TimelineVM.TimelineType.CHANNEL_VIDEOS);
            bundle.putParcelable("channel", channel);
            bundle.putString("peertube_instance", channel.getHost());
            bundle.putBoolean("sepia_search", sepiaSearch);
            displayVideosFragment.setArguments(bundle);
            return displayVideosFragment;
        }


        @Override
        public int getCount() {
            return 1;
        }
    }

}
