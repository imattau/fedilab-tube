package app.fedilab.fedilabtube.helper;
/* Copyright 2020 Thomas Schneider
 *
 * This file is a part of TubeLab
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * TubeLab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with TubeLab; if not,
 * see <http://www.gnu.org/licenses>. */

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public class HelperAcadInstance {


    public static Map<String, String> instances_themes;

    public static String LOCAL = "LOCALES";
    public static String DISCOVER = "DECOUVRIR";
    public static String MOSTLIKED = "PLUS_APPRECIEES";
    public static String RECENTLY_ADDED = "AJOUTE_RECEMMENT";
    public static String TRENDING = "TENDANCE";
    public static String HISTORY = "HISTORIQUE";
    public static String SUBSCRIPTIONS = "ABONNEMENTS";
    public static String MYVIDEOS = "VIDEOS";

    //List of available emails
    public static String[] valideEmails = {
            "ac-aix-marseille.fr",
            "ac-amiens.fr",
            "ac-besancon.fr",
            "ac-bordeaux.fr",
            "clermont-ferrand.fr",
            "ac-corse.fr",
            "ac-creteil.fr",
            "ac-dijon.fr",
            "ac-grenoble.fr",
            "education.fr",
            "ac-guadeloupe.fr",
            "ac-guyane.fr",
            "ac-reunion.fr",
            "ac-lille.fr",
            "ac-limoges.fr",
            "ac-lyon.fr",
            "ac-martinique.fr",
            "ac-mayotte.fr",
            "ac-montpellier.fr",
            "ac-nancy.fr",
            "ac-nantes.fr",
            "ac-normandie.fr",
            "ac-orleans-tours.fr",
            "ac-paris.fr",
            "ac-poitiers.fr",
            "ac-rennes.fr",
            "ac-spm.fr",
            "ac-strasbourg.fr",
            "ac-toulouse.fr",
            "ac-versailles.fr",
            "ac-wf.wf",
            "monvr.pf",
            "ac-noumea.nc",
            "education.gouv.fr",
            "igesr.gouv.fr"
    };

    static {
        Map<String, String> is = new LinkedHashMap<>();
        is.put("Normandie", "tube-normandie.beta.education.fr");
        is.put("Enseignement professionnel", "tube-enseignement-professionnel.apps.education.fr");
        is.put("Action éducative", "tube-action-educative.apps.education.fr");
        is.put("Numérique éducatif", "tube-numerique-educatif.apps.education.fr");
        is.put("Institutionnel", "tube-institutionnelle.apps.education.fr");
        is.put("2d - langues vivantes", "tube-2d-langues-vivantes.apps.education.fr");
        is.put("2d - éducation physique et sportive", "tube-2d-education-physique-et-sportive.apps.education.fr");
        is.put("2d - arts, lettres et sciences humaines", "tube-2d-arts-lettres-sciences-humaines.apps.education.fr");
        is.put("Maternelle", "tube-maternelle.apps.education.fr");
        is.put("Cycle 2", "tube-cycle-2.apps.education.fr");
        is.put("Cycle 3", "tube-cycle-3.apps.education.fr");
        instances_themes = Collections.unmodifiableMap(is);
    }

}
