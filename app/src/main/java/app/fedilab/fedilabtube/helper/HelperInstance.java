package app.fedilab.fedilabtube.helper;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Locale;

import app.fedilab.fedilabtube.BuildConfig;

/* Copyright 2020 Thomas Schneider
 *
 * This file is a part of TubeLab
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * TubeLab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with TubeLab; if not,
 * see <http://www.gnu.org/licenses>. */
public class HelperInstance {


    /**
     * Returns the instance of the authenticated user
     *
     * @param context Context
     * @return String domain instance
     */
    @SuppressWarnings("ConstantConditions")
    public static String getLiveInstance(Context context) {
        final SharedPreferences sharedpreferences = context.getSharedPreferences(Helper.APP_PREFS, Context.MODE_PRIVATE);
        String acad;
        if (BuildConfig.FLAVOR.compareTo("fdroid_full") == 0 || BuildConfig.FLAVOR.compareTo("google_full") == 0) {
            return sharedpreferences.getString(Helper.PREF_INSTANCE, getDefaultInstance());
        } else if (BuildConfig.FLAVOR.compareTo("bittube") == 0) {
            return sharedpreferences.getString(Helper.PREF_INSTANCE, "bittube.video");
        } else if (BuildConfig.FLAVOR.compareTo("queermotion") == 0) {
            return sharedpreferences.getString(Helper.PREF_INSTANCE, "queermotion.org");
        } else {
            acad = sharedpreferences.getString(Helper.PREF_INSTANCE, "tube-institutionnelle.apps.education.fr");
            if (acad == null) {
                acad = "tube-institutionnelle.apps.education.fr";
            }
            return acad;
        }
    }

    /**
     * Get a default instance host name depending of the device locale
     *
     * @return peertube host String
     */
    private static String getDefaultInstance() {
        String lang = Locale.getDefault().getLanguage();
        if (lang.contains("-")) {
            if (!lang.split("-")[0].trim().toLowerCase().startsWith("zh")) {
                lang = lang.split("-")[0];
                if (lang.split("-")[1].toLowerCase().contains("be")) {
                    lang = "be";
                } else if (lang.split("-")[1].toLowerCase().contains("gb")) {
                    lang = "gb";
                }
            } else {
                lang = lang.split("-")[0] + "-" + lang.split("-")[1].toUpperCase();
            }
        }
        switch (lang) {
            case "it":
                return "peertube.uno";
            case "be":
                return "peertube.be";
            case "fr":
                return "video.liberta.vip";
            case "de":
                return "peertube.at";
            case "ru":
                return "peertube.su";
            case "gb":
                return "peertube.co.uk";
            case "da":
            case "sv":
            case "nb":
            case "fi":
            case "is":
                return "peertube.dk";
            default:
                return "peertube.social";
        }
    }
}
