package app.fedilab.fedilabtube.helper;

import com.fasterxml.jackson.annotation.JsonProperty;

import su.litvak.chromecast.api.v2.Request;

public class DashCastRequest implements Request {
    @JsonProperty
    final String url;
    @JsonProperty
    final boolean force;
    @JsonProperty
    final boolean reload;
    @JsonProperty("reload_time")
    final int reloadTime;

    private Long requestId;

    public DashCastRequest(String url,
                           boolean force,
                           boolean reload,
                           int reloadTime) {
        this.url = url;
        this.force = force;
        this.reload = reload;
        this.reloadTime = reloadTime;
    }

    @Override
    public Long getRequestId() {
        return requestId;
    }

    @Override
    public void setRequestId(Long requestId) {
        this.requestId = requestId;
    }
}