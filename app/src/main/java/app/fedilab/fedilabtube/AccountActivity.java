package app.fedilab.fedilabtube;
/* Copyright 2020 Thomas Schneider
 *
 * This file is a part of TubeLab
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * TubeLab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with TubeLab; if not,
 * see <http://www.gnu.org/licenses>. */

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import org.jetbrains.annotations.NotNull;

import app.fedilab.fedilabtube.client.RetrofitPeertubeAPI;
import app.fedilab.fedilabtube.client.data.AccountData.Account;
import app.fedilab.fedilabtube.databinding.ActivityAccountBinding;
import app.fedilab.fedilabtube.fragment.DisplayAccountsFragment;
import app.fedilab.fedilabtube.fragment.DisplayChannelsFragment;
import app.fedilab.fedilabtube.fragment.DisplayNotificationsFragment;
import app.fedilab.fedilabtube.helper.Helper;
import app.fedilab.fedilabtube.helper.SwitchAccountHelper;
import app.fedilab.fedilabtube.sqlite.AccountDAO;
import app.fedilab.fedilabtube.sqlite.Sqlite;


public class AccountActivity extends AppCompatActivity {


    private ActivityAccountBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityAccountBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        SpannableString content_create = new SpannableString(getString(R.string.join_peertube));
        content_create.setSpan(new UnderlineSpan(), 0, content_create.length(), 0);
        content_create.setSpan(new ForegroundColorSpan(ContextCompat.getColor(AccountActivity.this, Helper.getColorAccent())), 0, content_create.length(),
                Spanned.SPAN_INCLUSIVE_EXCLUSIVE);


        SQLiteDatabase db = Sqlite.getInstance(getApplicationContext(), Sqlite.DB_NAME, null, Sqlite.DB_VERSION).open();
        SharedPreferences sharedpreferences = getSharedPreferences(Helper.APP_PREFS, MODE_PRIVATE);
        String token = sharedpreferences.getString(Helper.PREF_KEY_OAUTH_TOKEN, null);

        Account account = new AccountDAO(AccountActivity.this, db).getAccountByToken(token);
        if (account == null) {
            Helper.logoutCurrentUser(AccountActivity.this, null);
            return;
        }


        setTitle(String.format("@%s", account.getUsername()));

        Helper.loadGiF(AccountActivity.this, account.getAvatar().getPath(), binding.profilePicture);
        binding.username.setText(String.format("@%s", account.getUsername()));
        binding.displayname.setText(account.getDisplayName());

        binding.instance.setText(account.getHost());
        binding.editProfile.setOnClickListener(v -> startActivity(new Intent(AccountActivity.this, MyAccountActivity.class)));

        binding.logoutButton.setOnClickListener(v -> {
            AlertDialog.Builder dialogBuilderLogoutAccount = new AlertDialog.Builder(AccountActivity.this);
            dialogBuilderLogoutAccount.setMessage(getString(R.string.logout_account_confirmation, account.getUsername(), account.getHost()));
            dialogBuilderLogoutAccount.setPositiveButton(R.string.action_logout, (dialog, id) -> {
                Helper.logoutCurrentUser(AccountActivity.this, account);
                dialog.dismiss();
            });
            dialogBuilderLogoutAccount.setNegativeButton(R.string.cancel, (dialog, id) -> dialog.dismiss());
            AlertDialog alertDialogLogoutAccount = dialogBuilderLogoutAccount.create();
            alertDialogLogoutAccount.show();
        });

        binding.settings.setOnClickListener(v -> {
            Intent intent = new Intent(AccountActivity.this, SettingsActivity.class);
            startActivity(intent);
        });



        if (Helper.isLoggedIn(AccountActivity.this)) {
            binding.accountTabLayout.addTab(binding.accountTabLayout.newTab().setText(getString(R.string.title_notifications)));
            binding.accountTabLayout.addTab(binding.accountTabLayout.newTab().setText(getString(R.string.title_muted)));
            binding.accountTabLayout.addTab(binding.accountTabLayout.newTab().setText(getString(R.string.title_channel)));

            binding.accountViewpager.setOffscreenPageLimit(3);


            binding.accountViewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    TabLayout.Tab tab = binding.accountTabLayout.getTabAt(position);
                    if (tab != null)
                        tab.select();
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });


            binding.accountTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    binding.accountViewpager.setCurrentItem(tab.getPosition());
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {
                    Fragment fragment = null;
                    if (binding.accountViewpager.getAdapter() != null)
                        fragment = (Fragment) binding.accountViewpager.getAdapter().instantiateItem(binding.accountViewpager, tab.getPosition());
                    switch (tab.getPosition()) {
                        case 0:
                            if (fragment != null) {
                                DisplayNotificationsFragment displayNotificationsFragment = ((DisplayNotificationsFragment) fragment);
                                displayNotificationsFragment.scrollToTop();
                            }
                            break;
                        case 1:
                            if (fragment != null) {
                                DisplayAccountsFragment displayAccountsFragment = ((DisplayAccountsFragment) fragment);
                                displayAccountsFragment.scrollToTop();
                            }
                            break;
                        case 2:
                            if (fragment != null) {
                                DisplayChannelsFragment displayChannelsFragment = ((DisplayChannelsFragment) fragment);
                                displayChannelsFragment.scrollToTop();
                            }
                            break;
                    }
                }
            });

            PagerAdapter mPagerAdapter = new AccountsPagerAdapter(getSupportFragmentManager());
            binding.accountViewpager.setAdapter(mPagerAdapter);
        } else {
            binding.accountTabLayout.setVisibility(View.GONE);
            binding.accountViewpager.setVisibility(View.GONE);
            binding.editProfile.setVisibility(View.GONE);
            binding.remoteAccount.setVisibility(View.VISIBLE);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                binding.remoteAccount.setText(Html.fromHtml(getString(R.string.remote_account_from, account.getSoftware()), Html.FROM_HTML_MODE_LEGACY));
            else
                binding.remoteAccount.setText(Html.fromHtml(getString(R.string.remote_account_from, account.getSoftware())));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(@NotNull Menu menu) {
        getMenuInflater().inflate(R.menu.main_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else if (item.getItemId() == R.id.action_add_account) {
            SwitchAccountHelper.switchDialog(AccountActivity.this, true);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    /**
     * Pager adapter for three tabs (notifications, muted, blocked)
     */
    private static class AccountsPagerAdapter extends FragmentStatePagerAdapter {

        AccountsPagerAdapter(FragmentManager fm) {
            super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        }

        @NotNull
        @Override
        public Fragment getItem(int position) {
            Bundle bundle = new Bundle();
            switch (position) {
                case 1:
                    DisplayAccountsFragment displayAccountsFragment = new DisplayAccountsFragment();
                    bundle.putSerializable("accountFetch", RetrofitPeertubeAPI.DataType.MUTED);
                    displayAccountsFragment.setArguments(bundle);
                    return displayAccountsFragment;
                case 2:
                    return new DisplayChannelsFragment();
                default:
                    return new DisplayNotificationsFragment();
            }
        }

        @Override
        public int getCount() {
            return 3;
        }
    }


}