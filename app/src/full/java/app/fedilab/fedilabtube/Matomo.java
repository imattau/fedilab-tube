package app.fedilab.fedilabtube;
/* Copyright 2020 Thomas Schneider
 *
 * This file is a part of TubeLab
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * TubeLab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with TubeLab; if not,
 * see <http://www.gnu.org/licenses>. */


import android.content.Context;


@SuppressWarnings({"unused", "RedundantSuppression"})
public class Matomo {

    public static void sendScreen(Context _mcontext, String path, String title) {
        //Do nothing
    }

    public static void sendEvent(Context _mcontext, String category, String action, String label, float value) {
        //Do nothing
    }


    public static void sendValue(Context _mcontext, String path, int index, String dimensionValue) {
        //Do nothing
    }


    public static void trackInstall(Context _mcontext) {
        //Do nothing
    }
}
