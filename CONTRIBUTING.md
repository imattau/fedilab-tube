CONTRIBUTING
============

### Localizations:

TubeLab works only with [Crowdin](https://crowdin.com/project/tubelab), which offers nice tools for helping in translations.
New translations will be automatically merged in a branch.
If your language is not listed, please ask me to add it. If you prefer to work on an XML file, you should be able [to upload it with Crowdin](https://support.crowdin.com/xml-configuration/).
Crowdin will not pick up changes in develop branch, that's why all translations should be done with this tool.

### Issues:

Issues are handled on Github at: https://github.com/stom79/TubeLab/issues, before opening an issue, please check it has not yet been submitted by someone else

### Contribution to code:

Your contributions are welcomed, but please, use this repo https://framagit.org/tom79/fedilab-tube. You can create PR to the dev branch.