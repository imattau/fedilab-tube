Fixes some issues on 1.10.0 (Crashes when adding a playlist + bad behavior when subscribing to remote accounts)
Added:
- Chromecast support (default disabled)
- Detect start time in URLs

Changed:
- Instance picker supports URLs

Fixed:
- Typo
- Comment feature when logged out
- Full-screen breaks
- Crashes with the download button on live streams
- Jumps with full-screen and vertical videos
- Abuse report notifications clickable
- Remote channel subscriptions need twice clicks