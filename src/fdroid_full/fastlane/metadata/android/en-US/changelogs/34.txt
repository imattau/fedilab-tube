Added:
- Allow to connect Mastodon or Pleroma accounts
- Videos can be reblogged, added to favorites or bookmarked (with undo actions)
- For torrent mode: display download, upload rates with number of seeders
- Watermarks (from plugins) are displayed at the top left of videos.

Changed:
- Restrict videos to 5 tags
- Change message when an instance is not available

Fixed:
- Some crashes