*Nicht authentifizierter Modus*

Es handelt sich um einen eingeschränkten Modus, in dem Sie einige Aktionen ausführen können:

- Instanz wechseln,
- Videos teilen,
- Videos herunterladen.


*Authentifizierter Modus*

In diesem Modus sind viele Funktionen verfügbar:

- Kommentare schreiben/löschen
- Videos hochladen/entfernen/bearbeiten
- Verwalten (erstellen/bearbeiten/entfernen) von Kanälen und Wiedergabelisten
- Kanäle verfolgen/entfolgen
- Daumen hoch/runter
- Benachrichtigungen prüfen
- Kanäle stummschalten/Stummschaltung aufheben
- Videos/Accounts melden
- Überprüfe deinen Verlauf